<?php

/*
 * This file is part of Contao Kix Bundle.
 *
 * (c) Pronego
 *
 */

namespace Pronego\ContaoKixBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class ContaoKixBundle extends Bundle
{
}

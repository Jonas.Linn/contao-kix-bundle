
var x = window.matchMedia("(max-width: 1150px)");
x.addListener(prepare);
prepare(x);
var open = false;
var open_Desktop = false;
var selected_entry = '';

$( "#nav" ).click(function (event) {
  if(x.matches){
  	if(!open){
  	  $( "#nav .page-nav-list>li" ).css('display', 'block');
	  open = true;
  	}else{
      prepareMobile();
  	}
  }
});

function prepare(x){
	if(x.matches){
		prepareMobile();
	}else{
		prepareDesktop();
	}
}


function prepareDesktop(){
	$( "#nav .page-nav-list>li" ).css('display', 'inline-block');
    $('.page-nav-list>li>a').removeClass('selected');
    $('ul.article-nav').css('display', 'none');
	open_Desktop = false;

}

function prepareMobile(){
	$( "#nav .page-nav-list>li" ).css('display', 'none');
	$( "#nav .page-nav-list>li.first" ).css('display', 'block');
	open = false;
}


$("#nav>.page-nav>.page-nav-list>li>a").click(function(e){
	if(x.matches && !open){
		e.preventDefault();
	}
});

$("body:not(.mobile) #nav>.page-nav>.page-nav-list>li>a").hover(showNav);
$(".mobile #nav>.page-nav>.page-nav-list>li>a").click(showNav);

function showNav(e){
    if(!x.matches){
        if($('ul.'+$(this).data('alias')).length > 0){
            if(!open_Desktop){
                e.preventDefault();
                $('ul.'+$(this).data('alias')).css('display', 'block');
                selected_entry = $(this).data('alias');
                //$('ul.'+$(this).data('alias')).fadeIn(2000);
                open_Desktop = true;
                $(this).addClass('selected');
            }else{
                if(selected_entry != $(this).data('alias')){
                    $('*[data-alias='+selected_entry+']').removeClass('selected')
                    e.preventDefault();
                    $('ul.article-nav').css('display', 'none')
                    $('ul.'+$(this).data('alias')).css('display', 'block');
                    selected_entry = $(this).data('alias');
                    $(this).addClass('selected');
                }
            }
        }else{
            if(selected_entry != $(this).data('alias')){ //!!!!
                $('*[data-alias='+selected_entry+']').removeClass('selected');
                e.preventDefault();
                $('ul.article-nav').css('display', 'none')
                selected_entry = $(this).data('alias');
                $(this).addClass('selected');
            }
        }
    }
}

$("body:not(.mobile) #nav").hover(function(e){}, function(e) {
    if(open_Desktop) {
        $('ul.article-nav').css('display', 'none');
        open_Desktop = false;
        $('.page-nav-list>li>a').removeClass('selected');
    }else if(open){
        prepareMobile();
    }
});

$("body.mobile :not(#nav)").click(function(e) {
    if(e.target.closest("#nav") == null && open_Desktop) {
        $('ul.article-nav').css('display', 'none')
        open_Desktop = false;
        $('.page-nav-list>li>a').removeClass('selected');
    }else if(e.target.closest("#nav") == null && open){
        prepareMobile();
    }
});

/*
$("body.mobile").click(function(e) {
    alert('HI');
	if(e.target.closest("#nav") == null && open_Desktop) {
		$('ul.article-nav').css('display', 'none')
        open_Desktop = false;
        $('.page-nav-list>li>a').removeClass('selected');
	}else if(e.target.closest("#nav") == null && open){
		prepareMobile();
	}
});*/

//Sticky navbar
window.onscroll = function() {myFunction()};
var navbar = document.getElementById("nav");
var sticky = (navbar.offsetTop - 5);

function myFunction() {

    if (window.pageYOffset >= sticky) {
        navbar.classList.add("sticky")
    } else {
        navbar.classList.remove("sticky");
    }
}
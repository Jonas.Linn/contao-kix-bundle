require('../scss/general.scss')
require('../scss/tiles.scss');
require('../scss/product_page.scss');
require('../scss/navbar.scss');
require('../scss/footernav.scss');
require('../scss/business.scss');
require('../scss/references.scss');
require('../scss/customer_rating.scss');
require('../scss/rsts_nav.scss');
require('../scss/homeslider.scss');
require('../scss/start-text.scss');
require('../scss/text.scss');
require('../scss/advantages.scss');
require('../scss/button.scss');
require('../scss/events.scss');
require('../scss/backgrounds.scss');
require('../scss/hint.scss');
require('../scss/job-offer.scss');
require('../scss/picture.scss');
require('../scss/score.scss');
require('../scss/contact-btn.scss');
require('../scss/democenter_hint.scss');
require('../scss/news.scss');
require('../scss/accordionSingle.scss');
require('../scss/form_test.scss');
require('../scss/search.scss');
require('../scss/top-hint.scss');
require('../scss/distribution.scss');
require('../scss/downloads.scss');
require('../scss/intro.scss');
require('../scss/text_board.scss');
require('../scss/contact.scss');
require('../scss/contactbuttons.scss');
require('../scss/scrolldownarrow.scss');
require('../scss/scrolluparrow.scss');
require('../scss/company.scss');
require('../scss/dienstleistungen.scss');
require('../scss/gallery.scss');


require('./tile_slider.js');
require('./navbar.js');
require('./references.js');
require('./form_test.js');
require('./advantages.js')
require('./register_auto_select');



//document.getElementById("nav").addEventListener("click", myFunction);


var x = window.matchMedia("(max-width: 768px)");
x.addListener(prepare);
var extended = false;
var bg_minus = "background-image : url('data:image/svg+xml;charset=utf8, <svg aria-hidden=\"true\" focusable=\"false\" data-prefix=\"fas\" data-icon=\"minus\" class=\"svg-inline--fa fa-minus fa-w-14\" role=\"img\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 448 512\"><path fill=\"%2378BE5A\" d=\"M416 208H32c-17.67 0-32 14.33-32 32v32c0 17.67 14.33 32 32 32h384c17.67 0 32-14.33 32-32v-32c0-17.67-14.33-32-32-32z\"></path></svg>');background-size: 20px 20px; background-repeat: no-repeat; background-position: center;";
var bg_plus = "background-image : url('data:image/svg+xml;charset=utf8, <svg aria-hidden=\"true\" focusable=\"false\" data-prefix=\"fas\" data-icon=\"plus\" class=\"svg-inline--fa fa-plus fa-w-14\" role=\"img\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 448 512\"><path fill=\"%2378BE5A\" d=\"M416 208H272V64c0-17.67-14.33-32-32-32h-32c-17.67 0-32 14.33-32 32v144H32c-17.67 0-32 14.33-32 32v32c0 17.67 14.33 32 32 32h144v144c0 17.67 14.33 32 32 32h32c17.67 0 32-14.33 32-32V304h144c17.67 0 32-14.33 32-32v-32c0-17.67-14.33-32-32-32z\"></path></svg>');background-size: 20px 20px; background-repeat: no-repeat; background-position: center;";
prepare(x);


$( ".references_extend" ).click(function (event) {
  if(x.matches){
  	if(!extended){
        $( ".all_references" ).css('height', '520px');
        //$( ".references_extend" ).html('-');
        $( ".references_extend" ).attr('style', bg_minus);
        extended = true;
	}else{
        $( ".all_references" ).css('height', '280px');
        //$( ".references_extend" ).html('+');
        $( ".references_extend" ).attr('style', bg_plus);
        extended = false;
	}
  }
});

function prepare(x){
	if(x.matches){
		prepareMobile();
	}else{
		prepareDesktop();
	}
}


function prepareDesktop(){
    $( ".all_references" ).css('height', 'auto');
}

function prepareMobile(){
    $( ".all_references" ).css('height', '280px');
    //$( ".references_extend" ).html('+');
    $( ".references_extend" ).html('');
    $( ".references_extend" ).attr('style', bg_plus);
    extended = false;
}

//document.getElementById("nav").addEventListener("click", openNav);

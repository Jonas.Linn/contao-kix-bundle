$('input[type=radio][name="demotype"]').change(function(){
    $('.overlay-second.fieldset_overlay').addClass('hidden')
});

$('input[type=radio][name="testdata"]').change(function(){
    $('.overlay-third.fieldset_overlay').addClass('hidden')
});

$( document ).ready(function(){
    //Make sure the overlays are removed after an error prevented submitting the form
    if($('.widget.error').length > 0){
        $('.overlay-second.fieldset_overlay').addClass('hidden');
        $('.overlay-third.fieldset_overlay').addClass('hidden');
    }
});

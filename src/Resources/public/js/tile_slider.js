// Enable mobile slide for nonslider container 

var d = document;
var wrap = d.querySelector('.ce_rsce_tiles');
var items = d.querySelector('.tile_container');
var itemCount = d.querySelectorAll('.tile').length;
var scroller = d.querySelector('.scroller');
var pos = 0; 

// Calculate slide width, items and itemCount need the same width
function setTransform() { 
  items.style['transform'] = 'translate(' + (-pos * items.offsetWidth) + 'px,0)';
}

function prev() {
  pos = Math.max(pos - 1, 0);
  setTransform();
}

function next() {
  pos = Math.min(pos + 1, itemCount - 1);
  setTransform();
}

window.addEventListener('resize', setTransform);

$("#tile-slide-left").click(prev);
$("#tile-slide-right").click(next);
//document.getElementById("tile-slide-left").addEventListener("click", prev);
//document.getElementById("tile-slide-right").addEventListener("click", next);



 
 


var x = window.matchMedia("(min-width: 768px)");
var tc = window.matchMedia("(max-width: 1150px)");
x.addListener(prepare);
tc.addListener(show_hide_button);
var extended = false;
var bg_minus = "background-image : url('data:image/svg+xml;charset=utf8, <svg aria-hidden=\"true\" focusable=\"false\" data-prefix=\"fas\" data-icon=\"minus\" class=\"svg-inline--fa fa-minus fa-w-14\" role=\"img\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 448 512\"><path fill=\"%2378BE5A\" d=\"M416 208H32c-17.67 0-32 14.33-32 32v32c0 17.67 14.33 32 32 32h384c17.67 0 32-14.33 32-32v-32c0-17.67-14.33-32-32-32z\"></path></svg>');background-size: 20px 20px; background-repeat: no-repeat; background-position: center;";
var bg_plus = "background-image : url('data:image/svg+xml;charset=utf8, <svg aria-hidden=\"true\" focusable=\"false\" data-prefix=\"fas\" data-icon=\"plus\" class=\"svg-inline--fa fa-plus fa-w-14\" role=\"img\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 448 512\"><path fill=\"%2378BE5A\" d=\"M416 208H272V64c0-17.67-14.33-32-32-32h-32c-17.67 0-32 14.33-32 32v144H32c-17.67 0-32 14.33-32 32v32c0 17.67 14.33 32 32 32h144v144c0 17.67 14.33 32 32 32h32c17.67 0 32-14.33 32-32V304h144c17.67 0 32-14.33 32-32v-32c0-17.67-14.33-32-32-32z\"></path></svg>');background-size: 20px 20px; background-repeat: no-repeat; background-position: center;";
prepare(x);
show_hide_button(tc);
var max_height;


$( ".advantages .tiles_extend" ).click(function (event) {
  if(x.matches){
  	if(!extended){
  	    var tiles = $( ".advantages .tile" ).size();
        //var tc = window.matchMedia("(max-width: 1150px)");

        var height = Math.ceil(tiles/3)
        if(tc.matches){
            height = Math.ceil(tiles/2)
        }
        height = (max_height+50)*height;

        $( ".advantages .tile_container" ).css('height', height+"px");
        $( ".advantages .tiles_extend" ).attr('style', bg_minus);
        extended = true;
	}else{
        $( ".advantages .tile_container" ).css('height', (max_height+50)+'px');
        $( ".advantages .tiles_extend" ).attr('style', bg_plus);
        extended = false;
	}
  }
});

function prepare(x){
    var $tiles = $('.advantages .tile');
    max_height = 0;
    $tiles.each(function () {
        if($(this).outerHeight(false) > max_height){
            max_height = $(this).outerHeight(false);
        }
    });

    max_height = max_height - 30;

    $( ".advantages .tile" ).css('height', max_height+"px");

    window.max_height = max_height;

	if(!x.matches){
		prepareMobile();
	}else{
		prepareDesktop();
	}
}


function prepareDesktop(){
    $( ".advantages .tile_container" ).css('height', (max_height+50)+'px');
    $( ".advantages .tiles_extend" ).html('');
    $( ".advantages .tiles_extend" ).attr('style', bg_plus);
    extended = false;
}

function prepareMobile(){
    $( ".advantages .tile_container" ).css('height', (max_height+30)+'px');
}

function show_hide_button(tc) {
    var tiles = $( ".advantages .tile" ).size();

    if(tc.matches){
        if(tiles <= 2){
            $( ".advantages .tiles_extend" ).css('display', 'none');
        }else{
            $( ".advantages .tiles_extend" ).css('display', 'block');
        }
    }else{
        if(tiles <= 3){
            $( ".advantages .tiles_extend" ).css('display', 'none');
        }else{
            $( ".advantages .tiles_extend" ).css('display', 'block');
        }
    }
}

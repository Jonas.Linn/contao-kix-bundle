<?php

/*
 * This file is part of Contao Kix Bundle.
 *
 * (c) Pronego
 *
 */

namespace Pronego\ContaoKixBundle\Tests;

use Pronego\ContaoKixBundle\ContaoKixBundle;
use PHPUnit\Framework\TestCase;

class ContaoKixBundleTest extends TestCase
{
    public function testCanBeInstantiated()
    {
        $bundle = new ContaoKixBundle();

        $this->assertInstanceOf('Pronego\KixBundle\ContaoKixBundle', $bundle);
    }
}
